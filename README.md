# Geneibot
A personal bot for discord servers.

## Installing dependencies
Run `npm i`

### Running the bot
`npm start` is for local development and uses babel for nodeJS & pm2 is for running the bot on a server (DO box)

```js
npm start
```

```js
pm2 start bot.js
```

```js
pm2 save
```
