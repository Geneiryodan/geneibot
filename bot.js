/*====================================
=            Require core            =
====================================*/
const Discord = require("discord.js"),
      auth = require('./auth.json'),
      config = require("./config.js"),
      client = new Discord.Client();

/*====================================
=            Load plugins            =
====================================*/
const plugins = [];
config.plugins.forEach(pluginName => {
  const Plugin = require("./plugins/" + pluginName).default;
  plugins.push(new Plugin(client));
});

client.on("ready", () => {
	client.user.setActivity('With Geneiryodan')
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", msg => {
  plugins.forEach(plugin => plugin.handleMessage(msg));
});

client.login(auth.token);
