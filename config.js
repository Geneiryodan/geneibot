module.exports = {
  plugins: [
  	"loveCheck",
  	"botInfo",
  	"imageLock",
    "noSelfAdvertisement",
  	"eightBall",
  	"reactMe",
  	"rockPaperScissors",
  	"commandList",
  	"twitchSubscribe"
  	]
};
