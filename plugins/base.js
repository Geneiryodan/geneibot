import {RateLimiter} from 'limiter';

export default class BasePlugin {
  constructor(client) {
    this.client = client;
    this.prefix = '!';
    this.commands = {};
    this.limiter = new RateLimiter(4, 'minute');
  }

  handleMessage(msg) {
    if(msg.author.bot) return; // Ignore if the msg comes from the bot (prevents delete loop)

    // If the msg is not a command return false
    if (msg.content[0] !== this.prefix) {
      return false;
    }

    // Get the command
    const args = msg.content
      .slice(this.prefix.length)
      .trim()
      .split(/ +/g); // remove ! from msg.content and remove everything after spaces
    const command = args.shift().toLowerCase();

    if (!(command in this.commands)) {
      return false;
    }

    const removeToken = this.limiter.tryRemoveTokens(1);
    if(!removeToken) {
      let time = new Date().getSeconds(),
          seconds = 60 - time;

      msg.delete() // Delete !lovecheck command when posted by a user
        .then(msg => console.log(`Deleted message from ${msg.author.username}`))
        .catch(console.error);

      msg.reply(`the ${command} command is on cooldown for ${seconds} seconds!`)
      console.log(`the ${command} command is on cooldown for ${seconds} seconds!`)
    }

    this.commands[command](msg, args, removeToken);
  }
}
