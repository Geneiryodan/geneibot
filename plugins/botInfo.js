import BasePlugin from './base';

export default class GetBotInfo extends BasePlugin {
  constructor() {
    super(); // Extend from BasePlugin
    this.commands['botinfo'] = this.onBotInfo;
  }

  onBotInfo(msg, args, removeToken) {
    if(removeToken) {
        msg.reply(`I was created by this awesome guy => <@238748111554609154> at **17/06/2019** to make his life a bit easier :metal::skin-tone-2:`);
    }
  }
}
