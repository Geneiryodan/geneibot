import BasePlugin from './base';

export default class CommandList extends BasePlugin {
  constructor() {
    super(); // Extend from BasePlugin
    this.commands['commandlist'] = this.showCommands;
  }

  showCommands(msg) {
  	msg.reply("```!botinfo\n!emojis\n!subscribe\n!rps <rock|paper|scissors>\n!lovecheck <@username>\n!8ball <question>```")
  }
}
