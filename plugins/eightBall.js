import BasePlugin from './base';

export default class EightBall extends BasePlugin {
  constructor() {
    super(); // Extend from BasePlugin
    this.commands['8ball'] = this.onEightBall.bind(this);
    this.responses = [
    	`Whatever.. :thumbsup:`,
    	`I won't recommend it :joy:`,
    	`The answer is within yourself :crystal_ball:`,
    	`Are you joking??? :joy:`,
    	`I dont know.. ask <@238748111554609154> :wink:`,
    	`Sorry I dont know what 'drive also driving the ambulance means'`,
    	`Hmm.. not sure :thinking:`
    ]
  }

  onEightBall(msg, args, removeToken) {
  	if(args.length < 1) {
  		msg.reply('please ask a question e.g: `!8ball do you love chocolate`');
  		return false;
  	}

    if(msg.content.includes('Roast Macone')) {
      msg.reply("Roast Macone?! Without me he wouldn't exist :joy:")
      return false;
    }
  	
    if(removeToken) {            	
  		let randomResponse = this.responses[Math.floor(Math.random() * this.responses.length)]
  		msg.reply(randomResponse)
    }
  }
}
