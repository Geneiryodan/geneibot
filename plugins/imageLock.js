import BasePlugin from './base';
let msgSent;

export default class ImageLock extends BasePlugin {
  constructor(client) {
    super(client); // Extend from BasePlugin
    // this.commands['imagelock'] = this.onImageLock;
    this.client.on("message", this.onMessage.bind(this));
  }

  onMessage(msg) {
    const allowedRole = msg.guild.roles.find(role => role.name === "Mods");
    const coreCheck = msg.member.roles.has(allowedRole.id) && msg.channel.name === 'photos';
    // #photos live ID: 587413148458549249 | #bot-command debug ID: 581195651770220545

    if(coreCheck && msg.attachments.array().length < 1 && msg.embeds.length < 1) {
      if(msg.attachments.every(a => typeof a.width != 'number') || !msg.embeds.type === 'image') {
        if(msg.author.bot) return; // Ignore if the msg comes from the bot (prevents delete loop)

        msg.delete()
          .then(msg => console.log(`Deleted message from ${msg.author.username}`))
          .catch(console.error);

        if(!msgSent) {
          msg.reply('This channel is images only')
            .then(msg => {
              msgSent = true; // msgSent = true so it wont send another bot message
              msg.delete(2000);
            })
            .catch(console.error)
            .finally(msgSent = false) // When promise done => msgSent = false so the bot can resend message if needed
        }
      }
    }
  }
}
