import BasePlugin from './base';

export default class LovePlugin extends BasePlugin {
  constructor() {
    super(); // Extend from BasePlugin
    this.commands['lovecheck'] = this.onLoveCheck;
  }

  onLoveCheck(msg, args, removeToken) {
    const seedrandom = require('seedrandom');

    if (args.length < 1) {
      msg.reply('please enter a username. e.g: `!lovecheck @GeneiBot`');
      return false;
    }

    if(removeToken) {
        const date = new Date().toJSON().slice(0, 13); // Day, Month, Year, Hour (new lovecheck every hour)
        let mention = args[0]; // @UserMention
        let rng = seedrandom(`${msg.author.username} ${mention} ${date}`);
        msg.channel.send(`There's **${Math.floor(rng() * 100)}% love** chance between ${msg.author} & ${mention} :heart:`);
    }
  }
}
