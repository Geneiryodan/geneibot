import BasePlugin from './base';
let msgSent;

export default class NoSelfAdvertisement extends BasePlugin {
  constructor(client) {
    super(client); // Extend from BasePlugin
    this.client.on("message", this.onMessage.bind(this));
  }

  onMessage(msg) {
    const allowedRole = msg.guild.roles.find(role => role.name === "Mods");
    const coreCheck = !msg.member.roles.has(allowedRole.id);

    if(coreCheck && msg.content.includes('twitch.tv/')) {
      if(!msg.content.includes('twitch.tv/arieltv_') || !msg.content.includes('twitch.tv/ArielTV_')) {
        if(msg.author.bot) return; // Ignore if the msg comes from the bot (prevents delete loop)

        msg.delete()
          .then(msg => console.log(`Deleted message from ${msg.author.username}`))
          .catch(console.error);

        if(!msgSent) {
          msg.reply('Self advertisement is not allowed!')
            .then(msg => {
              msgSent = true; // msgSent = true so it wont send another bot message
              msg.delete(4000);
            })
            .catch(console.error)
            .finally(function() {msgSent = false}) // When promise done => msgSent = false so the bot can resend message if needed
        }
      }
    }
  }
}
