import BasePlugin from './base';

export default class ReactMe extends BasePlugin {
  constructor() {
    super(); // Extend from BasePlugin
    this.commands['emojis'] = this.onReact;
  }

  onReact(msg, args, removeToken) {
    if(removeToken) {
    	// Use Promise.all instead of .then() because emoji react order doesnt matter
    	Promise.all([
    		msg.react('589034518283943946'), //Kappa
    		msg.react('583270800023355428'), //LUL
			msg.react('586121313945649182'), //Kreygasm
			msg.react('🍇'),
    	]).catch(() => console.error('One of the emojis failed to react.'));        
    }
  }
}
