import BasePlugin from './base';

export default class RockPaperScissors extends BasePlugin {
  constructor() {
    super(); // Extend from BasePlugin
    this.commands['rps'] = this.onRPS.bind(this);
    this.values = [
    	`rock`,
    	`paper`,
    	`scissors`
    ];
    this.emoji = '';
    this.reactEmoji = '';
  }

  onRPS(msg, args, removeToken) {
  	if(args.length < 1) {
  		msg.reply('Please add an argument e.g: `!rps scissors`');
  		return false;
  	}

  	if(!args.includes('rock') && !args.includes('paper') && !args.includes('scissors')) {  	
  		msg.reply('You are only allowed to use the following arguments: `rock, paper, scissors`');
  		return false;
  	}

  	if(removeToken) {
  		let userChoice = this.values.indexOf(args[0]);
  		let rng = Math.floor(Math.random() * this.values.length);
  		let botChoice = this.values[rng];

  		// t = tie, u = user wins, b = bot wins
  		const results = [
  		['t', 'b', 'u'],
  		['u', 't', 'b'],
  		['b', 'u', 't'],
  		]; 

  		let outcome = results[userChoice][rng];
  		botChoice = botChoice.charAt(0).toUpperCase() + botChoice.slice(1) // Capitalize bot choice for msg.reply

  		// Define what emoji should be used in msg.reply
  		if(botChoice === 'Rock') {
  			this.emoji = ':moyai:';
  		} else if(botChoice === 'Paper') {
  			this.emoji = ':newspaper2:'
  		} else {
  			this.emoji = ':scissors:'
  		}

  		// Define what emoji should be used by the bot to msg.react
  		if(outcome === 't') {
  			this.reactEmoji = '583270800023355428';
  		} else if(outcome === 'u') {
  			this.reactEmoji = ':slight_frown:';
  		} else {
  			this.reactEmoji = '589034518283943946';
  		}

  		const resultMap = {
  		't': `It's a tie! :warning: you both drew ${botChoice} ${this.emoji}`,
  		'u': `Won! :muscle: <@590091399832141824> drew ${botChoice} ${this.emoji}`,
  		'b': `You lost! :slight_frown: <@590091399832141824> drew ${botChoice} ${this.emoji}`
  		};

  		msg.reply(resultMap[outcome]).then(botMsg => {
  			botMsg.react(this.reactEmoji);
  		}).catch(botMsg => console.error('Couldnt post emoji'));
  	}
  }
}
