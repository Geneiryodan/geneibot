import BasePlugin from './base';
const embed = {
    	color: 0xFF0099,
    	title: 'Twitch Subscription',
    	url: 'https://www.twitch.tv/subs/arieltv_',
    	author: {
    		name: 'ArielTV_',
    		icon_url: 'https://static-cdn.jtvnw.net/jtv_user_pictures/75aa3a12-3802-485d-9e7b-55c1e5c061c6-profile_image-70x70.png',
    		url: 'https://twitch.tv/arieltv_',
    	},
    	description: 'Support ArielTV by subscribing to het twitch channel! \n visit https://twitch.tv/subs/arieltv_ to subscribe!\n \n And get access to the following emotes & sub badges:',
    	image: {
    		url: 'https://panels-images.twitch.tv/panel-273996202-image-42cbd5d6-78c6-4eb8-a5a7-5a185a0f7ca3',
    	},
    	timestamp: new Date(),
    	footer: {
    		text: 'Be sure to tune in!',
    		icon_url: 'https://static-cdn.jtvnw.net/jtv_user_pictures/75aa3a12-3802-485d-9e7b-55c1e5c061c6-profile_image-70x70.png',
    	},
    };

export default class CommandList extends BasePlugin {
  constructor() {
    super(); // Extend from BasePlugin
    this.commands['subscribe'] = this.showSubscribeUrl;
  }

  showSubscribeUrl(msg) {
  	msg.reply({embed: embed})
  }
}